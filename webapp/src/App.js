//libs
import React from "react";
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "@apollo/react-hooks";

// Custom components
import CenterPanel from "./components/CenterPanel/CenterPanel";
import AppHeader from "./components/AppHeader/AppHeader";
import RaidEventContainer from "./components/RaidEvent/RaidEventContainer";

// Styling
import "./App.css";

const client = new ApolloClient({
    uri: "http://localhost:4000/graphql",
});

function App() {
    return (
        <ApolloProvider client={client}>
            <div className="app">
                <AppHeader/>
                <CenterPanel/>
            </div>
        </ApolloProvider>
    );
}

function About() {
    return (
        <div>
            <h2>About</h2>
        </div>
    )
}

export default App;
