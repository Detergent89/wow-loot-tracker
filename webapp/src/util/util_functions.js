export const capitalizeFirstLetter = (string) => {
  let newString = string.toLowerCase();
  return newString.charAt(0).toUpperCase() + newString.slice(1);
};

export const sqlTimestampToDate = (timeStampString) => {
  let splitDateString = timeStampString.split(/[- T:.]/);

  let date = new Date(
    Date.UTC(
      splitDateString[0],
      splitDateString[1]-1, //Months are 0-11 in Javascript
      splitDateString[2],
      splitDateString[3],
      splitDateString[4],
      splitDateString[5]
    )
  );

  if (Object.prototype.toString.call(date) === "[object Date]") {
    return date;
  }
};
