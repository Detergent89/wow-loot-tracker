import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

const FETCH_ROLES = gql`
  {
    __type(name: "Role") {
      name
      enumValues {
        name
      }
    }
  }
`;

const useStyles = makeStyles(() => ({
    formControl: {
        minWidth: 120,
    },
}));

const capitalizeFirstLetter = (string) => {
    let newString = string.toLowerCase();
    return newString.charAt(0).toUpperCase() + newString.slice(1);
};

function RoleSelector(props) {
    const { loading, error, data } = useQuery(FETCH_ROLES);
    const [state, setState] = React.useState({ selectedRole: "" });
    const roles = useStyles();

    let menuItems;

    if (loading || error) {
        menuItems = [];
    } else {
        menuItems = data.__type.enumValues;
    }

    const helperText = props.isErrorState ? "Required" : "";

    return (
        <div style={{ margin: "0 10px" }}>
            <FormControl className={roles.formControl} error={props.isErrorState}>
                <InputLabel id="role-select-dropdown-label">Role</InputLabel>
                <Select
                    labelId="role-select-dropdown-label"
                    id="role-select-dropdown"
                    value={state.selectedRole}
                    onChange={(e) => {
                        props.handleRoleSelection(e.target.value);
                        setState({ selectedRole: e.target.value });
                    }}
                >
                    {menuItems.map((item) => (
                        <MenuItem key={item.name} value={item.name}>
                            {capitalizeFirstLetter(item.name)}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{helperText}</FormHelperText>
            </FormControl>
        </div>
    );
}

export default RoleSelector;
