import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

const FETCH_INSTANCES = gql`
  {
    raid_instance {
      id
      abbreviation
    }
  }
`;

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 120,
  },
}));

function InstanceSelector(props) {
  const { loading, error, data } = useQuery(FETCH_INSTANCES);
  const [state, setState] = React.useState({ selectedInstance: "" });
  const classes = useStyles();

  let instances;

  if (loading || error) {
    instances = [];
  } else {
    instances = data.raid_instance;
  }

  const helperText = props.isErrorState ? "Required" : "";

  return (
    <div style={{ margin: "0 10px" }}>
      <FormControl className={classes.formControl} error={props.isErrorState}>
        <InputLabel id="instance-select-dropdown-label">Instance</InputLabel>
        <Select
          labelId="instance-select-dropdown-label"
          id="instance-select-dropdown"
          value={state.selectedInstance}
          onChange={(e) => {
            props.handleInstanceSelection(e.target.value);
            setState({ selectedInstance: e.target.value });
          }}
        >
          {instances.map((instance) => (
            <MenuItem key={instance.id} value={instance.id}>
              {instance.abbreviation}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{helperText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export default InstanceSelector;
