import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

import { capitalizeFirstLetter } from "../../util/util_functions"

const FETCH_CLASSES = gql`
  {
    __type(name: "Class") {
      name
      enumValues {
        name
      }
    }
  }
`;

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 120,
  },
}));

function ClassSelector(props) {
  const { loading, error, data } = useQuery(FETCH_CLASSES);
  const [state, setState] = React.useState({ selectedClass: "" });
  const classes = useStyles();

  let menuItems;

  if (loading || error) {
    menuItems = [];
  } else {
    menuItems = data.__type.enumValues;
  }

  const helperText = props.isErrorState ? "Required" : "";

  return (
    <div style={{ margin: "0 10px" }}>
      <FormControl className={classes.formControl} error={props.isErrorState}>
        <InputLabel id="class-select-dropdown-label">Class</InputLabel>
        <Select
          labelId="class-select-dropdown-label"
          id="class-select-dropdown"
          value={state.selectedClass}
          onChange={(e) => {
            props.handleClassSelection(e.target.value);
            setState({ selectedClass: e.target.value });
          }}
        >
          {menuItems.map((item) => (
            <MenuItem key={item.name} value={item.name}>
              {capitalizeFirstLetter(item.name)}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{helperText}</FormHelperText>
      </FormControl>
    </div>
  );
}

export default ClassSelector;
