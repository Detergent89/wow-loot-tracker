import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import makeStyles from "@material-ui/core/styles/makeStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

const FETCH_ALTFLAGS = gql`
  {
    __type(name: "AltFlag") {
      name
      enumValues {
        name
      }
    }
  }
`;

const useStyles = makeStyles(() => ({
    formControl: {
        minWidth: 120,
    },
}));

const capitalizeFirstLetter = (string) => {
    let newString = string.toLowerCase();
    return newString.charAt(0).toUpperCase() + newString.slice(1);
};

function AltFlagSelector(props) {
    const { loading, error, data } = useQuery(FETCH_ALTFLAGS);
    const [state, setState] = React.useState({ selectedAltFlag: "" });
    const altFlag = useStyles();

    let menuItems;

    if (loading || error) {
        menuItems = [];
    } else {
        menuItems = data.__type.enumValues;
    }

    const helperText = props.isErrorState ? "Required" : "";

    return (
        <div style={{ margin: "0 10px" }}>
            <FormControl className={altFlag.formControl} error={props.isErrorState}>
                <InputLabel id="altFlag-select-dropdown-label">Status</InputLabel>
                <Select
                    labelId="altFlag-select-dropdown-label"
                    id="altFlag-select-dropdown"
                    value={state.selectedAltFlag}
                    onChange={(e) => {
                        props.handleAltFlagSelection(e.target.value);
                        setState({ selectedAltFlag: e.target.value });
                    }}
                >
                    {menuItems.map((item) => (
                        <MenuItem key={item.name} value={item.name}>
                            {capitalizeFirstLetter(item.name)}
                        </MenuItem>
                    ))}
                </Select>
                <FormHelperText>{helperText}</FormHelperText>
            </FormControl>
        </div>
    );
}

export default AltFlagSelector;
