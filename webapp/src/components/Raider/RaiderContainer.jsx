import React, {Fragment, useState} from "react";

import RaiderList from "./RaiderList";
import RaiderCreationForm from "./RaiderCreationForm";

function RaiderContainer() {
  const [needsRefresh, setState] = useState(false);

  const signalRefresh = () => {
    //console.debug("Signalling refresh");
    setState(true);
  };

  const signalRefreshDone = () => {
    //console.debug("Signalling refresh done");
    setState(false);
  };

  return (
      <Fragment>
        <h2>Raider Creation</h2>
        <RaiderCreationForm signalRefresh={signalRefresh}/>
        <RaiderList
            needsRefresh={needsRefresh}
            signalRefreshDone={signalRefreshDone}
        />
      </Fragment>
  );
}

export default RaiderContainer;
