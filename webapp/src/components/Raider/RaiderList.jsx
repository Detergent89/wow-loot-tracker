import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import { usePrevious } from "../../hooks/UsePrevious";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const FETCH_RAIDERS = gql`
  {
    raider {
      id
      name
      characterClass: class
      characterRole: role
      characterAltFlag: altFlag
    }
  }
`;

function RaiderList(props) {
  const { loading, error, data, refetch } = useQuery(FETCH_RAIDERS);

  // Get the previous value (was passed into hook on last render)
  const previousNeedsRefresh = usePrevious(props.needsRefresh);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  if (props.needsRefresh && !previousNeedsRefresh) {
    refetch().then((T) => {
      //console.debug(data);
      props.signalRefreshDone();
    });
  }

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Character Name</TableCell>
          <TableCell>Class</TableCell>
          <TableCell>Role</TableCell>
          <TableCell>AltFlag</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.raider.map(({ id, name, characterClass, characterRole, characterAltFlag }) => (
          <TableRow key={id}>
            <TableCell>{name}</TableCell>
            <TableCell>{characterClass}</TableCell>
            <TableCell>{characterRole}</TableCell>
            <TableCell>{characterAltFlag}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default RaiderList;
