import React from "react";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import update from "immutability-helper";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";

import ClassSelector from "../Selector/ClassSelector";
import RoleSelector from "../Selector/RoleSelector";
import AltFlagSelector from "../Selector/AltFlagSelector";

const ADD_RAIDER = gql`
  mutation create_raider(
    $name: String!
    $class: Class!
    $role: Role!
    $altFlag: AltFlag!
  ) {
    create_raider(name: $name, class: $class, role: $role, altFlag: $altFlag) {
      id
      name
    }
  }
`;

const useStyles = makeStyles({
  wrapperDiv: {
    marginLeft: "16px",
  },
});

function RaiderCreationForm(props) {
  const classes = useStyles();
  const [currentState, setState] = React.useState({
    characterName: "",
    characterClass: {},
    characterRole: {},
    characterAltFlag: {},
    textInputError: false,
    textInputHelperText: "",
    classSelectionError: false,
  });

  const [addRaider] = useMutation(ADD_RAIDER);

  const handleSubmit = (e) => {
    e.preventDefault();

    const textInputError = currentState.characterName === "";
    const classSelectionError =
      Object.keys(currentState.characterClass).length === 0;
    const roleSelectionError =
      Object.keys(currentState.characterRole).length === 0;
    const altFlagSelectionError =
      Object.keys(currentState.characterAltFlag).length === 0;

    const newState = update(currentState, {
      textInputError: { $set: textInputError },
      textInputHelperText: { $set: textInputError ? "Required" : "" },
      classSelectionError: { $set: classSelectionError },
      roleSelectionError: { $set: roleSelectionError },
      altFlagSelectionError: { $set: altFlagSelectionError },
    });

    setState(newState);

    if (!textInputError && !classSelectionError) {
      addRaider({
        variables: {
          name: currentState.characterName,
          class: currentState.characterClass,
          role: currentState.characterRole,
          altFlag: currentState.characterAltFlag,
        },
      })
        .then(() => {
          props.signalRefresh();
        })
        .catch((errors) => {
          errors.graphQLErrors.forEach((err) => {
            if (err.message === "ER_DUP_ENTRY") {
              const newState = update(currentState, {
                textInputError: { $set: true },
                textInputHelperText: {
                  $set: "A raider with that name already exists!",
                },
              });
              setState(newState);
            }
          });
        });
    }
  };

  const handleClassSelection = (classEnum) => {
    const newState = update(currentState, {
      classSelectionError: { $set: false },
      characterClass: { $set: classEnum },
    });
    setState(newState);
  };

  const handleRoleSelection = (roleEnum) => {
    const newState = update(currentState, {
      roleSelectionError: { $set: false },
      characterRole: { $set: roleEnum },
    });
    setState(newState);
  };

  const handleAltFlagSelection = (altFlagEnum) => {
    const newState = update(currentState, {
      altFlagSelectionError: { $set: false },
      characterAltFlag: { $set: altFlagEnum },
    });
    setState(newState);
  };

  const formStyle = { display: "flex", flexDirection: "row" };

  return (
    <div className={classes.wrapperDiv}>
      <form
        style={formStyle}
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <TextField
          id="standard-basic"
          label="Character Name"
          value={currentState.characterName}
          error={currentState.textInputError}
          helperText={currentState.textInputHelperText}
          onChange={(e) => {
            const newState = update(currentState, {
              characterName: { $set: e.target.value },
              textInputError: { $set: false },
              textInputHelperText: { $set: "" },
            });
            setState(newState);
          }}
        />
        <ClassSelector
          handleClassSelection={handleClassSelection}
          isErrorState={currentState.classSelectionError}
        />
        <RoleSelector
          handleRoleSelection={handleRoleSelection}
          isErrorState={currentState.roleSelectionError}
        />
        <AltFlagSelector
          handleAltFlagSelection={handleAltFlagSelection}
          isErrorState={currentState.altFlagSelectionError}
        />
        <Button type="submit" variant="contained" style={{ margin: "5px" }}>
          Add Raider
        </Button>
      </form>
    </div>
  );
}

export default RaiderCreationForm;
