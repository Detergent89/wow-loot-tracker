import React from "react";
import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import DateFnsUtils from "@date-io/date-fns";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import update from "immutability-helper";

import InstanceSelector from "../Selector/InstanceSelector";

//CREATE InstanceSelector per above - similar to class/role Selector

const ADD_RAID_EVENT = gql`
  mutation create_raid_event($date: DateTime!, $instanceId: String!) {
    create_raid_event(date: $date, instanceId: $instanceId) {
      start_time
      raid_instance {
        abbreviation
      }
    }
  }
`;

const useStyles = makeStyles({
  wrapperDiv: {
    marginLeft: "16px",
  },
});

function RaidEventCreationForm(props) {
  const classes = useStyles();

  const [currentState, setState] = React.useState({
    raidDate: new Date(),
    raidInstance: {},
    textInputError: false,
    textInputHelperText: "",
    instanceSelectionError: false,
  });

  const [addRaidEvent] = useMutation(ADD_RAID_EVENT);

  const handleSubmit = (e) => {
    e.preventDefault();

    let textInputError = currentState.raidDate === "";
    let instanceSelectionError =
      Object.keys(currentState.raidInstance).length === 0;

    const newState = update(currentState, {
      textInputError: { $set: textInputError },
      instanceSelectionError: { $set: instanceSelectionError },
      textInputHelperText: { $set: textInputError ? "Required" : "" },
    });

    setState(newState);

    if (!textInputError && !instanceSelectionError) {
      addRaidEvent({
        variables: {
          date: currentState.raidDate,
          instanceId: currentState.raidInstance,
        },
      })
        .then((data) => {
          props.signalRefresh();
        })
        .catch((errors) => {
          errors.graphQLErrors.forEach((err) => {
            if (err.message === "ER_DUP_ENTRY") {
              const newState = update(currentState, {
                textInputError: { $set: true },
                textInputHelperText: { $set: "Input error" },
              });
              setState(newState);
            }
          });
        });
    }
  };

  const handleDateChange = (newDate) => {
    const newState = update(currentState, {
      raidDate: { $set: newDate },
    });
    setState(newState);
  };
  const handleInstanceSelection = (instanceEnum) => {
    const newState = update(currentState, {
      raidInstance: { $set: instanceEnum },
      instanceSelectionError: { $set: false },
    });
    setState(newState);
  };

  const formStyle = { display: "flex", flexDirection: "row" };

  return (
    <div className={classes.wrapperDiv}>
      <form
        style={formStyle}
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DatePicker
            label="Raid Date"
            value={currentState.raidDate}
            onChange={handleDateChange}
          />
        </MuiPickersUtilsProvider>

        <InstanceSelector
          handleInstanceSelection={handleInstanceSelection}
          isErrorState={currentState.instanceSelectionError}
        />
        <Button type="submit" variant="contained" style={{ margin: "5px" }}>
          Add Raid Event
        </Button>
      </form>
    </div>
  );
}

export default RaidEventCreationForm;
