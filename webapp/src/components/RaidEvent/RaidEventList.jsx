import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

import { sqlTimestampToDate } from "../../util/util_functions";
import { usePrevious } from "../../hooks/UsePrevious";
import { makeStyles } from "@material-ui/core/styles";

const FETCH_RAID_EVENTS = gql`
  {
    raid_event {
      id
      name
      start_time
      raid_instance {
        abbreviation
      }
    }
  }
`;

const useStyles = makeStyles({
  table: {
    overflow: "auto",
  },
});

function descendingByDate(raid1, raid2) {
  const raid1Start = sqlTimestampToDate(raid1.start_time);
  const raid2Start = sqlTimestampToDate(raid2.start_time);

  // Descending order by date
  if (raid2Start.getTime() > raid1Start.getTime()) {
    return 1;
  } else if (raid2Start.getTime() < raid1Start.getTime()) {
    return -1;
  } else {
    return 0;
  }
}

function RaidEventList(props) {
  const classes = useStyles();

  const { loading, error, data, refetch } = useQuery(FETCH_RAID_EVENTS);

  const previousNeedsRefresh = usePrevious(props.needsRefresh);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  if (props.needsRefresh && !previousNeedsRefresh) {
    refetch().then((T) => {
      //console.debug(data);
      props.signalRefreshDone();
    });
  }

  return (
    <Table className={classes.table}>
      <TableHead>
        <TableRow>
          <TableCell>Instance</TableCell>
          <TableCell>Start Time</TableCell>
          <TableCell>Raid Name</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.raid_event.sort(descendingByDate).map((raidEvent) => (
          <TableRow key={raidEvent.id}>
            <TableCell>{raidEvent.raid_instance.abbreviation}</TableCell>
            <TableCell>
              {sqlTimestampToDate(raidEvent.start_time).toLocaleString()}
            </TableCell>
            <TableCell>{raidEvent.name}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export default RaidEventList;
