import React, {Fragment, useState} from "react";

import RaidEventList from "./RaidEventList";
import RaidEventCreationForm from "./RaidEventCreationForm";

function RaidEventContainer() {
    const [needsRefresh, setState] = useState(false);

    const signalRefresh = () => {
        //console.debug("Signalling refresh");
        setState(true);
    };

    const signalRefreshDone = () => {
        //console.debug("Signalling refresh done");
        setState(false);
    };

    return (
        <Fragment>
            <h2>Raid Event Creation</h2>
            <RaidEventCreationForm signalRefresh={signalRefresh}/>
            <RaidEventList
                needsRefresh={needsRefresh}
                signalRefreshDone={signalRefreshDone}
            />
        </Fragment>
    );
}

export default RaidEventContainer;
