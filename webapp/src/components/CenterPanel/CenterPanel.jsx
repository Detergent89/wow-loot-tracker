import React from "react";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import { makeStyles } from "@material-ui/core/styles";

import RaidEventContainer from "../RaidEvent/RaidEventContainer";
import RaiderContainer from "../Raider/RaiderContainer";
import TabPanel from "../TabPanel/TabPanel";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles({
  container: {
    height: "80%",
  },
  centerPanel: {
    padding: "0 10px",
    overflow: "auto",
    height: "100%",
  },
});

function CenterPanel() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Container maxWidth="md" className={classes.container}>
      <Card className={classes.centerPanel} variant="outlined">
        <Tabs value={value} onChange={handleChange}>
          <Tab label="Raids" {...a11yProps(0)}></Tab>
          <Tab label="Raider Roster" {...a11yProps(1)}></Tab>
        </Tabs>
        <TabPanel value={value} index={0}>
          <RaidEventContainer />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <RaiderContainer />
        </TabPanel>
      </Card>
    </Container>
  );
}

export default CenterPanel;
