import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";

const useStyles = makeStyles({
  appHeader: {
    height: "8%",
    marginBottom: "4%",
    background: "white",
    color: "black",
    flexDirection: "row",
    alignItems: "center",
  },
  siteName: {
    marginLeft: "2%",
  },
});

function AppHeader() {
  const classes = useStyles();
  return (
    <AppBar position="static" className={classes.appHeader}>
      <h3 className={classes.siteName}>WoW Loot Tracker</h3>
    </AppBar>
  );
}

export default AppHeader;
