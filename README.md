# Getting Started
- Install docker
- Install node modules in both the /api and /webapp directories
- Run "docker-compose up" in the project root folder (this takes a few minutes the first time)
- Open localhost:3000 for the webapp, or localhost:4000/graphql for the api