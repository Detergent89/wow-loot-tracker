// Lib
const express = require("express");
const graphqlHTTP = require("express-graphql");
const cors = require("cors");

const schema = require("./schema");

const PORT = 4000;
const HOST = "0.0.0.0";

var corsOptions = {
  origin: "http://localhost:3000",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

var app = express();

app.use(cors(corsOptions));

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);

app.listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);
