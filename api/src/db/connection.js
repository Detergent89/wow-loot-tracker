//https://github.com/mysqljs/mysql
const mysql = require("mysql");
const db_config = require("../config/dbconfig");

const pool = mysql.createPool(db_config);

const connection = {
  getPool: function () {
    return pool;
  },
  get: function (callback) {
    pool.getConnection(function (err, connection) {
      callback(err, connection);
    });
  },
};

module.exports = connection;
