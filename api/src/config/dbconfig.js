const db_config = {
    host     : 'db',
    user     : 'schema_user',
    password : 'password',
    database : 'wowloottracker'
};

module.exports = db_config;