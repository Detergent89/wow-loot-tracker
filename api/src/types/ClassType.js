const { GraphQLEnumType } = require("graphql");

const ClassType = new GraphQLEnumType({
  name: "Class",
  values: {
    DRUID: {
      value: "Druid",
    },
    HUNTER: {
      value: "Hunter",
    },
    MAGE: {
      value: "Mage",
    },
    PALADIN: {
      value: "Paladin",
    },
    PRIEST: {
      value: "Priest",
    },
    ROGUE: {
      value: "Rogue",
    },
    SHAMAN: {
      value: "Shaman",
    },
    WARLOCK: {
      value: "Warlock",
    },
    WARRIOR: {
      value: "Warrior",
    },
  },
});

module.exports = ClassType;
