const { GraphQLNonNull, GraphQLObjectType, GraphQLString, GraphQLInt } = require("graphql");

const ItemType = new GraphQLObjectType({
  name: "Item",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) }
  }),
});

module.exports = ItemType;
