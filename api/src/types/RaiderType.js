const { GraphQLNonNull, GraphQLObjectType, GraphQLString } = require("graphql");

const RaiderType = new GraphQLObjectType({
  name: "Raider",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLString) },
    name: { type: GraphQLNonNull(GraphQLString) },
    class: { type: GraphQLNonNull(GraphQLString) },
    role: { type: GraphQLString },
    altFlag: { type: GraphQLString },
  }),
});

module.exports = RaiderType;
