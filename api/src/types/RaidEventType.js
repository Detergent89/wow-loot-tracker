const { GraphQLNonNull, GraphQLObjectType, GraphQLString } = require("graphql");
const { GraphQLDateTime } = require("graphql-iso-date");

const RaidInstanceType = require("../types/RaidInstanceType");
const raidInstanceQueryResolver = require("../resolvers/raidInstance/raidInstanceQueryResolver");

//TODO JMG figure out how we're actually supposed to share a connection pool between resolvers/types
const db = require("../db/connection");
const dbPool = db.getPool();

const RaidEventType = new GraphQLObjectType({
  name: "RaidEvent",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLString) },
    name: { type: GraphQLString },
    start_time: { type: GraphQLNonNull(GraphQLDateTime) },
    end_time: { type: GraphQLDateTime },
    raid_instance_id: { type: GraphQLString },
    raid_instance: {
      type: RaidInstanceType,
      resolve(parent) {
        return raidInstanceQueryResolver(this, dbPool, {
          id: parent.raid_instance_id,
        }).then((value) => {
          return value[0];
        });
      },
    },
  }),
});

module.exports = RaidEventType;
