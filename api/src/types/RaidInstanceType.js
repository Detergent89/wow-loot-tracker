const { GraphQLNonNull, GraphQLObjectType, GraphQLString } = require("graphql");

const RaidInstanceType = new GraphQLObjectType({
  name: "RaidInstance",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLString) },
    name: { type: GraphQLNonNull(GraphQLString) },
    abbreviation: { type: GraphQLNonNull(GraphQLString) },
  }),
});

module.exports = RaidInstanceType;
