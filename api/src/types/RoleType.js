const { GraphQLEnumType } = require("graphql");

const RoleType = new GraphQLEnumType({
    name: "Role",
    values: {
        TANK: {
            value: "Tank",
        },
        MELEE: {
            value: "Melee DPS",
        },
        RANGED: {
            value: "Ranged DPS",
        },
        HEALER: {
            value: "Healer",
        },
    },
});

module.exports = RoleType;
