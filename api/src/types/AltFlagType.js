const { GraphQLEnumType } = require("graphql");

const AltFlagType = new GraphQLEnumType({
    name: "AltFlag",
    values: {
        MAIN: {
            value: "Main",
        },
        ALT: {
            value: "Alt",
        },
    }
});

module.exports = AltFlagType;
