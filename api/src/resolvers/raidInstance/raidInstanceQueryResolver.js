function queryForId(pool, args) {
  return new Promise((resolve, reject) => {
    pool.query("SELECT * FROM raid_instance WHERE id = ?", [args.id], function (
      error,
      results
    ) {
      if (error) reject(error);
      resolve(results);
    });
  });
}

function queryForAll(pool) {
  return new Promise((resolve, reject) => {
    pool.query("SELECT * FROM raid_instance", function (error, results) {
      if (error) reject(error);
      resolve(results);
    });
  });
}

const resolvers = (callback, pool, args) => {
  return args.id ? queryForId(pool, args) : queryForAll(pool);
};

module.exports = resolvers;
