const { v4: uuidv4 } = require("uuid");

const resolvers = (callback, pool, args) => {
  return new Promise((resolve, reject) => {
    const newRaiderId = uuidv4();
    pool.getConnection(function (err, connection) {
      if (err) reject(err);
      connection.query(
        "INSERT INTO raider (id, name, class, role, altFlag) VALUES (?,?,?,?,?)",
        [newRaiderId, args.name, args.class, args.role, args.altFlag],
        function (err) {
          if (err) reject(err);
          connection.query(
            "SELECT * FROM raider WHERE id = ?",
            [newRaiderId],
            function (err, results) {
              if (err) reject(err);
              connection.release();
              resolve(results);
            }
          );
        }
      );
    });
  });
};

module.exports = resolvers;
