function queryForId(pool, args) {
  return new Promise((resolve, reject) => {
    pool.query("SELECT * FROM item WHERE id = ?", [args.id], function (
      error,
      results,
      fields
    ) {
      if (error) reject(error);
      resolve(results);
    });
  });
}

function queryForName(pool, args) {
  return new Promise((resolve, reject) => {
    pool.query("SELECT * FROM item WHERE name like %?%", [args.name], function (
      error,
      results,
      fields
    ) {
      if (error) reject(error);
      resolve(results);
    });
  });
}
  
function queryForAll(pool) {
  return new Promise((resolve, reject) => {
    pool.query("SELECT * FROM item", function (error, results, fields) {
      if (error) reject(error);
      resolve(results);
    });
  });
}
  
const resolvers = (callback, pool, args) => {
  var query = null;
  
  if(args.name) {
    query = queryForName(pool, args);
  } else if(args.id) {
    query = queryForId(pool, args);
  } else {
    query = queryForAll(pool);
  }

  return query;
};

module.exports = resolvers;
  