const { v4: uuidv4 } = require("uuid");

const resolvers = (callback, pool, args) => {
  return new Promise((resolve, reject) => {
    const newRaidEventId = uuidv4();
    pool.getConnection(function (err, connection) {
      if (err) reject(err);
      connection.query(
        "INSERT INTO raid_event (id, start_time, raid_instance_id) VALUES (?,?,?)",
        [newRaidEventId, args.date, args.instanceId],
        function (err) {
          if (err) reject(err);
          connection.query(
            "SELECT * FROM raid_event WHERE id = ?",
            [newRaidEventId],
            function (err, results) {
              if (err) reject(err);
              connection.release();
              resolve(results);
            }
          );
        }
      );
    });
  });
};

module.exports = resolvers;
