// Lib
const {
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
} = require("graphql");
const { GraphQLDateTime } = require("graphql-iso-date");

const db = require("./db/connection");

// Types
const ClassType = require("./types/ClassType");
const ItemType = require("./types/ItemType");
const RaiderType = require("./types/RaiderType");
const RaidEventType = require("./types/RaidEventType");
const RaidInstanceType = require("./types/RaidInstanceType");
const RoleType = require("./types/RoleType");
const AltFlagType = require("./types/AltFlagType");

// Resolvers
const itemQueryResolver = require("./resolvers/item/itemQueryResolver");
const raiderQueryResolver = require("./resolvers/raider/raiderQueryResolver");
const raiderMutatorResolver = require("./resolvers/raider/raiderMutatorResolver");
const raidEventQueryResolver = require("./resolvers/raidEvent/raidEventQueryResolver");
const raidEventMutatorResolver = require("./resolvers/raidEvent/raidEventMutatorResolver");
const raidInstanceQueryResolver = require("./resolvers/raidInstance/raidInstanceQueryResolver");

const dbPool = db.getPool();

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    raider: {
      type: new GraphQLList(RaiderType),
      args: {
        id: {
          description: "id of the raider",
          type: GraphQLString,
        },
      },
      resolve(parent, args) {
        return raiderQueryResolver(this, dbPool, args).then((value) => value);
      },
    },
    raid_event: {
      type: new GraphQLList(RaidEventType),
      args: {
        id: {
          description: "id of the raid_event",
          type: GraphQLString,
        },
      },
      resolve(parent, args) {
        return raidEventQueryResolver(this, dbPool, args).then(
          (value) => value
        );
      },
    },
    raid_instance: {
      type: new GraphQLList(RaidInstanceType),
      args: {
        id: {
          description: "id of the raid_instance type",
          type: GraphQLString,
        },
      },
      resolve(parent, args) {
        return raidInstanceQueryResolver(this, dbPool, args).then(
          (value) => value
        );
      },
    },
    item: {
      type: new GraphQLList(ItemType),
      args: {
        id: {
          description: "id of the item",
          type: GraphQLInt,
        },
        name: {
          description: "name of the item",
          type: GraphQLString,
        },
      },
      resolve(parent, args) {
        return itemQueryResolver(this, dbPool, args).then((value) => value);
      },
    },
  },
});

const RootMutation = new GraphQLObjectType({
  name: "RootMutationType",
  fields: {
    create_raider: {
      type: RaiderType,
      args: {
        name: {
          type: GraphQLNonNull(GraphQLString),
        },
        class: {
          type: GraphQLNonNull(ClassType),
        },
        role: {
          type: GraphQLNonNull(RoleType),
        },
        altFlag: {
          type: GraphQLNonNull(AltFlagType),
        },
      },
      resolve(parent, args) {
        return raiderMutatorResolver(this, dbPool, args)
          .then((value) => value[0])
          .catch((error) => {
            if (error.code === "ER_DUP_ENTRY") {
              throw new Error(error.code);
            }
            throw new Error(error);
          });
      },
    },
    create_raid_event: {
      type: RaidEventType,
      args: {
        date: {
          type: GraphQLDateTime,
        },
        instanceId: {
          type: GraphQLNonNull(GraphQLString),
        },
      },
      resolve(parent, args) {
        return raidEventMutatorResolver(this, dbPool, args)
          .then((value) => value[0])
          .catch((error) => {
            if (error.code === "ER_DUP_ENTRY") {
              throw new Error(error.code);
            }
            throw new Error(error);
          });
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation,
  types: [RaiderType, RaidEventType],
});
