SET @BWL_Test_Raid_Event_ID = (SELECT id FROM raid_event WHERE name = 'MC Test Raid');

SET @Rhogaz_ID = (SELECT id FROM raider WHERE name = 'Rhogaz');
SET @Crul_ID = 19363;
INSERT INTO loot_event (raider_id, item_id, raid_event_id)
    VALUES (@Rhogaz_ID, @Crul_ID, @BWL_Test_Raid_Event_ID);

SET @Vevise_ID = (SELECT id FROM raider WHERE name = 'Vevise');
SET @Tear_ID = 19379;
INSERT INTO loot_event (raider_id, item_id, raid_event_id)
    VALUES (@Vevise_ID, @Tear_ID, @BWL_Test_Raid_Event_ID);