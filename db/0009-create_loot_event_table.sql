CREATE TABLE loot_event (
    id VARCHAR(36) NOT NULL DEFAULT (UUID()),
    raider_id VARCHAR(36) NOT NULL,
    item_id int NOT NULL,
    raid_event_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (raider_id)
        REFERENCES raider(id)
        ON DELETE NO ACTION,
    FOREIGN KEY (item_id)
        REFERENCES item(id)
        ON DELETE NO ACTION
) ENGINE=INNODB;