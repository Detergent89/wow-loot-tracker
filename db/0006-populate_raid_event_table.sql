INSERT INTO raid_event (id, name, raid_instance_id)
    VALUES (UUID(), 'BWL Test Raid',
        (SELECT id FROM raid_instance WHERE abbreviation = 'BWL'));
INSERT INTO raid_event (id, name, raid_instance_id)
    VALUES (UUID(), 'MC Test Raid',
        (SELECT id FROM raid_instance WHERE abbreviation = 'MC'));