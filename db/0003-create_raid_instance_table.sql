CREATE TABLE raid_instance (
    id VARCHAR(36) NOT NULL DEFAULT (UUID()),
    name VARCHAR(256) NOT NULL,
    abbreviation VARCHAR(256) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name)
) ENGINE=INNODB;