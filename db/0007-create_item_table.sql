CREATE TABLE item (
    id int NOT NULL,
    name VARCHAR(256),
    PRIMARY KEY (id)
) ENGINE=INNODB;