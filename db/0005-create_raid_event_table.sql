CREATE TABLE raid_event (
    id VARCHAR(36) NOT NULL DEFAULT (UUID()),
    name VARCHAR(256),
    start_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    end_time TIMESTAMP,
    raid_instance_id VARCHAR(36),
    PRIMARY KEY (id),
    FOREIGN KEY (raid_instance_id)
        REFERENCES raid_instance(id)
        ON DELETE NO ACTION
) ENGINE=INNODB;