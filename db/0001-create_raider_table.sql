CREATE TABLE raider (
    id VARCHAR(36) NOT NULL DEFAULT (UUID()),
    name VARCHAR(256) NOT NULL,
    class ENUM (
        'Druid',
        'Hunter',
        'Mage',
        'Paladin',
        'Priest',
        'Rogue',
        'Shaman',
        'Warlock',
        'Warrior'
        ),
    role ENUM (
        'Tank',
        'Melee DPS',
        'Ranged DPS',
        'Healer'
        ),
    altFlag ENUM (
        'Main',
        'Alt'
        ),
    PRIMARY KEY (id),
    UNIQUE (name)
) ENGINE=INNODB;
