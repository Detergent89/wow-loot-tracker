# Import Items from a Wowhead Raid URL
# Sample Usage: 
# importItems.py -u https://classic.wowhead.com/blackwing-lair -c clientId -p clientSecret -f bwl.json
# Setup: 
# pip install requests
# pip install jsonpickle
# Requires:
# Battle.NET API Access
import sys
import requests
import re
import jsonpickle
from item import Item
from bnetApi import BnetApi
from optparse import OptionParser

usage = "usage: %prog -u wowHeadUrl -c bnetApiClientId -p bnetApiClientSecret -f jsonOutputFile"
parser = OptionParser(usage=usage)
parser.add_option("-f", "--file", dest="filename",
                  help="File to write JSON items to", metavar="FILE")
parser.add_option("-c", "--client-id", dest="clientId",
                  help="Battle.net API Client Id", metavar="ID")
parser.add_option("-p", "--client-secret", dest="clientSecret",
                  help="Battle.net API Client Secret", metavar="SECRET")
parser.add_option("-u", "--url", dest="url",
                  metavar="URL",
                  help="WowHead URL to scrape for items")


def parseOptions(args):
  (options, args) = parser.parse_args()

  if not options.filename:
    parser.error('--file is a required argument')

  if not options.clientId:
    parser.error('--client-id is a required argument')

  if not options.clientSecret:
    parser.error('--client-secret is a required argument')

  if not options.url:
    parser.error('--url is a required argument')

  return options


def getPageData(url):
  r = requests.get(url = url)
  return r

def getItemsFromLine(itemData):
  items = []
  itemMatches = re.findall(r'"(?P<itemId>\d+)":{"name_enus":"(?P<itemName>[^"]+)"', itemData, re.IGNORECASE)
  uniqueItems = {}
  if itemMatches:
    for match in itemMatches:
      itemKey = "%s-%s" % (match[0], match[1].strip())
      if itemKey not in uniqueItems:
        print("Item Id: %s Item Name: %s" % (match[0], match[1]))
        item = Item(match[0], match[1].strip())
        items.append(item)
      else:
        print("Skipping Item Id: %s Item Name: %s. Already in list." % (match[0], match[1]))
  return items


def parsePageData(pageData):
  allItems = []
  #Wowhead obfuscates the item data by loading it in later, with relatively obscure data
  #This line means it's adding in data, but it could be related to bosses, classes, items or various other things
  #Take this initial list and validate it against wowhead
  dataLineRegex = re.compile(r'WH\.Gatherer\.addData', re.IGNORECASE)
  lines = pageData.splitlines()

  for line in lines:
    match = dataLineRegex.match(line)
    if match:
      items = getItemsFromLine(line)
      allItems += items
  return allItems

def validateItems(items, api):
  validItems = []

  for item in items:
    try:
      print('Checking: %s - %s' % (item.id, item.name))
      bnetItem = api.getItem(item.id)
      if bnetItem:
        if bnetItem.equals(item):
          print('Item is valid!\n')
          validItems.append(bnetItem)
        else: 
          print('Item Mismatch: %s' % item.id)
          print('Wowhead: %s' % item.name)
          print('BattleNet: %s\n' % bnetItem.name)
      else:
        print('Not a valid item!\n')
    except Exception as ex:
       print("Error checking item %s. Error: %s" % (item.id, ex))
  return validItems

def serializeItems(items, file):
  with open(file, 'w') as jsonFile:
    jsonFile.write(jsonpickle.encode(items, indent=4))
  print("Serialized %s item(s) to %s" % (len(items), file))


def main(args):
  options = parseOptions(args)
  try:
    pageData = getPageData(options.url)
    items = parsePageData(pageData.text)

    api = BnetApi(options.clientId, options.clientSecret)
    api.initialize()
    
    items = validateItems(items, api)

    serializeItems(items, options.filename)
  except Exception as ex:
    print("Error fetching data from %s. Error: %s" % (options.url, ex))
    

if __name__ == "__main__":
    main(sys.argv)
