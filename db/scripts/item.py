class Item:
  def __init__(self, id, name):
    self.name = name
    self.id = int(id)

  def equals(self, item):
    if item is None:
      return False
    
    if self.id == item.id and self.name.lower() == item.name.lower():
      return True
    
    return False