import jsonpickle
import os
import sys
from item import Item
from optparse import OptionParser

usage = "usage: %prog -f jsonItemInputFile -o sqlOutputFile"
parser = OptionParser(usage=usage)
parser.add_option("-f", "--file", dest="filename",
                  help="JSON file containg items to insert", metavar="FILE")
parser.add_option("-o", "--output-file", dest="outputFilename",
                  help="Output SQL Insert File", metavar="FILE")


def parseOptions(args):
  (options, args) = parser.parse_args()

  if not options.filename:
    parser.error('--file is a required argument')

  if not options.outputFilename:
    options.outputFilename = os.path.splitext(options.filename)[0] + ".sql"
    print("--output-file was not specified. Output file will be: %s" % options.outputFilename)
  
  return options


def deserializeItems(file):
  json = None
  with open(file, 'r') as jsonFile:
    json = jsonFile.read()

  return jsonpickle.decode(json)


def convertToSql(items):
  itemInserts = []
  for item in items:
    sql = "INSERT IGNORE INTO item (id, name) VALUES (%s, '%s');\n" % (item.id, item.name.replace("'", "''"))
    itemInserts.append(sql)
  return itemInserts


def writeFile(file, lines):
  print("Writing to: %s" % file)
  with open(file, 'w') as sqlFile:
    sqlFile.writelines(lines)


def main(args):
  options = parseOptions(args)

  items = deserializeItems(options.filename)
  sqlLines = convertToSql(items)
  writeFile(options.outputFilename, sqlLines)

if __name__ == "__main__":
    main(sys.argv)