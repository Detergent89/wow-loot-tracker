import requests
from item import Item
from requests.auth import HTTPBasicAuth
#Request a Client Id and Client Secret here: https://develop.battle.net/access/clients
bnetTokenUrl = "https://us.battle.net/oauth/token"
bnetApiUrl = "https://us.api.blizzard.com/data/wow"
#Sample API Request: https://us.api.blizzard.com/data/wow/item/19019?namespace=static-classic-us&locale=en_US&access_token={accessToken}
class BnetApi:
  def __init__(self, clientId, clientSecret):
    self.__clientId = clientId
    self.__clientSecret = clientSecret

  def initialize(self):
    print("Fetching Battle.net credentials.")
    body = {"grant_type": 'client_credentials'}
    auth = HTTPBasicAuth(self.__clientId, self.__clientSecret)
    response = requests.post(url=bnetTokenUrl, data=body, auth=auth)
    if response.status_code == 200:
      jsonResponse = response.json()
      self.__accessToken = jsonResponse["access_token"]
      self.__tokenType = jsonResponse["token_type"]
      print("Battle.net credentials obtained!")
    else:
      response.raise_for_status()


  def getItem(self, itemId):
    item = None

    itemUrl= "%s/item/%s?namespace=static-classic-us&locale=en_US&access_token=%s" % (bnetApiUrl, itemId, self.__accessToken)
    response = requests.get(url=itemUrl)
    if response.status_code == 200:
      itemJson = response.json()
      item = Item(itemJson['id'], itemJson['name'].strip())
    elif response.status_code == 404:
      #item not found, it's simply not an item
      item = None
    else:
      response.raise_for_status()

    return item


